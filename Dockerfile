FROM centos/httpd-24-centos7 

USER root

RUN yum install rh-php72-php rh-php72-php-cli rh-php72-php-common rh-php72-php-curl rh-php72-php-gd rh-php72-php-intl rh-php72-php-json rh-php72-php-mbstring rh-php72-php-mcrypt rh-php72-php-mysqlnd rh-php72-php-opcache rh-php72-php-readline rh-php72-php-soap rh-php72-php-xml rh-php72-php-xmlrpc rh-php72-php-zip rh-php72-php-ldap augeas mariadb git python3-ldap python3 -y

RUN rpm -Uvh --nodeps $(repoquery --location mod_geoip) && \
    cp /usr/lib64/httpd/modules/mod_geoip.so /etc/httpd/modules

COPY git-pull.sh /usr/local/bin/git-pull.sh
COPY civicrm-cron.sh /usr/local/bin/civicrm-cron.sh 
COPY ipa-ca.crt /etc/ipa/ca.crt
COPY ldap.conf /etc/openldap/ldap.conf

RUN mkdir -p /srv/http/www.gnome.org; \
    mkdir /etc/ipa; \
    mkdir -p /home/admin/bin
COPY 00_www.gnome.org.conf /etc/httpd/conf.d

COPY gnome-custom.aug /tmp
RUN augtool -f /tmp/gnome-custom.aug

RUN groupadd wgo -g 1000370000 && \
    useradd wgo -g 1000370000 -u 1000370000 -G apache -r -l

RUN chown 1000370000:1000370000 -R /home/admin/bin

EXPOSE 8443

USER wgo
WORKDIR /srv/http/www.gnome.org
ENTRYPOINT ["/usr/bin/run-httpd"]
