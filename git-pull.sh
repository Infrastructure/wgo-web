#!/bin/bash

PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin"
THEME_PATH="/srv/http/www.gnome.org/gnome-web-www"

if [ -d "${THEME_PATH}" ]; then
    cd "${THEME_PATH}"

    git checkout -f 
    git pull
else
    echo "Unable to access the ${THEME_PATH} directory. Failing. \n"
    exit 1
fi
