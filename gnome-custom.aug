set /augeas/save overwrite
set /augeas/load/phpcustom/lens "PHP.lns"
set /augeas/load/phpcustom/incl "/etc/opt/rh/rh-php72/php.ini"
load
set /files/etc/opt/rh/rh-php72/php.ini/PHP/upload_max_filesize 500M
set /files/etc/opt/rh/rh-php72/php.ini/PHP/post_max_size 500M
set /files/etc/opt/rh/rh-php72/php.ini/PHP/max_execution_time 300
save
